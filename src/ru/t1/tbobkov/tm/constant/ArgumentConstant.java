package ru.t1.tbobkov.tm.constant;

public final class ArgumentConstant {

    private ArgumentConstant() {}

    public static final String HELP = "help";

    public static final String VERSION = "version";

    public static final String ABOUT = "about";

}
