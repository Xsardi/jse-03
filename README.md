# TASK MANAGER

## DEVELOPER INFO

* **Name**: Timur Bobkov
* **E-Mail**: tbobkov@t1-consulting.ru

## REQUIREMENTS

### SOFTWARE

* **OS**: WINDOWS 10 x64
* **JAVA**: OpenJDK version "1.8.0_345"

### HARDWARE

* **CPU**: i7 / Ryzen5
* **RAM**: 16GB
* **SSD**: 512GB

## PROGRAM RUN

```shell
java -jar task-manager.jar
```

